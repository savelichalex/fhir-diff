$(".contain-json-view").each(function(_, el) {
   $(el).on("click", function(event) {
       if(!/active/.test(event.currentTarget.className) &&
          !$(event.currentTarget).attr('data-gen-json-view')) {
           $(event.currentTarget.nextSibling)
               .find(".json-view")
               .each(function(_, el) {
                   $(el).jsonViewer(JSON.parse($(el).attr("data-json")), { collapsed: true});
               });
           $(event.currentTarget).attr('data-gen-json-view', true);
       }
   });
});

$(".filter").on("keyup", function(event) {
   var text = $(event.currentTarget).val();
   var textRegExp = new RegExp(text);
   $(".contain-json-view > span").each(function(_, el) {
      const id = el.innerHTML;
      if(textRegExp.test(id)) {
          $(el.parentNode.parentNode).removeClass("hidden");
      } else {
          $(el.parentNode.parentNode).addClass("hidden");
      }
   });
});