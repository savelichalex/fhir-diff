(defproject fhir-diff "0.1.0-SNAPSHOT"
  :description "Get diff from FHIR standart"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clj-json "0.5.3"]
                 [org.clojure/tools.cli "0.3.5"]
                 [hiccup "1.0.5"]
                 [garden "1.3.2"]]
  :resource-paths ["resources"]
  :main fhir-diff.core)
