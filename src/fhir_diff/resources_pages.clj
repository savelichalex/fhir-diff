(ns fhir-diff.resources-pages
  (:require [fhir-diff.generate-page :as gp]
            [clj-json.core :as json]
            [fhir-diff.regular-page :refer [gen-regular-diff-markup]]
            [fhir-diff.styles :as s]))

(defn gen-resources-list [items title]
  (when (not (empty? items))
    [:div
      title
     [:div.list-wrapper
      (into [:div.collection]
            (->> items
                 (map #(identity [:a.collection-item {:href (str % ".html")} %]))))]]))

(defn gen-resources-diff-markup [diff]
  (let [created (keys (:created diff))
        deleted (keys (:deleted diff))
        updated (keys (:updated diff))]
    [:div
     (gen-resources-list created [:p.status-header.first-header "Created"])
     (gen-resources-list deleted [:p.status-header "Deleted"])
     (gen-resources-list updated [:p.status-header "Updated"])]))

(defn generate-resources-page [diff]
  (gp/generate-page
    (gp/nav "resources")
    (gen-resources-diff-markup (:resources diff))))

(defn generate-created-resource-page [item entries]
  (gp/generate-page
    (gp/nav "resources" item)
    [:div
     [:p.status-header.first-header item]
     [:div.list-wrapper
      (gp/gen-collapsible
        entries
        #(identity [:span (get-in % ["resource" "id"])])
        #(identity [:pre.json-view {:data-json (json/generate-string %)}]))]]))

(defn generate-deleted-resource-page [item entries]
  (gp/generate-page
    (gp/nav "resources" item)
    [:div
     [:p.status-header.first-header item]
     [:div.list-wrapper
      (gp/gen-collapsible
        entries
        #(identity [:span (get-in % ["resource" "id"])])
        #(identity [:pre.json-view {:data-json (json/generate-string %)}]))]]))

(defn generate-updated-resource-page [item diff]
  (gp/generate-page
    (gp/nav "resources" item)
    (gen-regular-diff-markup diff)))