(ns fhir-diff.core
  (:require [clj-json.core :as json]
            [clojure.pprint :refer [pprint]]
            [clojure.java.io :refer [make-parents as-file resource]]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.edn :as edn]
            [fhir-diff.main-page :refer [generate-main-page]]
            [fhir-diff.regular-page :refer [generate-regular-page]]
            [fhir-diff.resources-pages :as res-page]
            [fhir-diff.diff :refer [get-fhir-diff]])
  (:gen-class))

(def cli-options
  [
   ["-c" "--calculate" "Need to calculate diff between versions"]
   ["-s" "--search" "Search by calculated diff"]
   ["-g" "--generate" "Generate static site with diff results"]
   ["-h" "--help"]
   ])

(defn exit [status msg]
  (println msg)
  (System/exit status))

(def ^:dynamic diff nil)
(declare ^:dynamic *locals*)

(defn eval-with-locals
  "Evals a form with given locals. The locals should be a map of symbols to
values."
  [locals form]
  (binding [*locals* locals]
    (eval
      `(let ~(vec (mapcat #(list % `(*locals* '~%)) (keys locals)))
         ~form))))

(defn search-repl [diff]
  (let [exit (partial exit 1 "Bye!")]
    (println (eval-with-locals {'diff diff 'exit exit} (read-string (read-line))))
    (search-repl diff)))

(defn get-entry-from-fhir-file [file]
  (let [json-from-file (json/parse-string file)]
    (get json-from-file "entry")))

(defn get-entry [folder file-name]
  (get-entry-from-fhir-file (slurp (str "./" folder "/" file-name ".json"))))

(defn -main
  "Entry point"
  [& args]
  (let [{:keys [options]} (parse-opts args cli-options)
        calculated-diff (atom nil)
        folder-to-save "FHIR-diff"
        file-to-save (str folder-to-save "/diff.edn")]
    (when (:calculate options)
      (do
        (println "Type folders with FHIR standart to calculate diff between versions")
        (let [new-folder (read-line)
              old-folder (read-line)
              fhir-diff (get-fhir-diff (partial get-entry new-folder) (partial get-entry old-folder))]
          (println (str "Save diff to \"" folder-to-save "\" folder"))
          (make-parents file-to-save)
          (spit file-to-save (prn-str fhir-diff))
          (println "Done")
          (swap! calculated-diff (fn [_] fhir-diff)))))
    (when (:search options)
      (when (nil? @calculated-diff)
        (if (not (.exists (as-file file-to-save)))
          (exit 1 "Need to calculate diff first. Use -c flag")
          (let [file (slurp file-to-save)
                fhir-diff (edn/read-string file)]
            (swap! calculated-diff (fn [_] fhir-diff)))))
      (println "Search by diff")
      (search-repl @calculated-diff))
    (when (:generate options)
      (when (nil? @calculated-diff)
        (if (not (.exists (as-file file-to-save)))
          (exit 1 "Need to calculate diff first. Use -c flag")
          (let [file (slurp file-to-save)
                fhir-diff (edn/read-string file)]
            (swap! calculated-diff (fn [_] fhir-diff)))))
      (let [site-folder-to-save (str folder-to-save "-site")
            main-page-file-to-save (str site-folder-to-save "/index.html")]
        (println (str "Generate static site to \"" site-folder-to-save "\" folder"))
        (make-parents main-page-file-to-save)
        (spit main-page-file-to-save (generate-main-page @calculated-diff))
        (doseq [[type val] @calculated-diff]
          (when (not= type :resources)
            (spit (str site-folder-to-save "/" (name type) ".html") (generate-regular-page (name type) val))))
        (spit (str site-folder-to-save "/resources.html") (res-page/generate-resources-page @calculated-diff))
        (doseq [[resource vals] (get-in @calculated-diff [:resources :created])]
          (spit (str site-folder-to-save "/" resource ".html") (res-page/generate-created-resource-page resource vals)))
        (doseq [[resource vals] (get-in @calculated-diff [:resources :deleted])]
          (spit (str site-folder-to-save "/" resource ".html") (res-page/generate-deleted-resource-page resource vals)))
        (doseq [[resource vals] (get-in @calculated-diff [:resources :updated])]
          (spit (str site-folder-to-save "/" resource ".html") (res-page/generate-updated-resource-page resource vals)))
        ; copy js files to site folder
        (let [json-viewer-css (str site-folder-to-save "/jquery.json-viewer.css")
              json-viewer-js (str site-folder-to-save "/jquery.json-viewer.js")
              common-js (str site-folder-to-save "/common.js")]
          (spit json-viewer-css (slurp (resource "jquery.json-viewer.css")))
          (spit json-viewer-js (slurp (resource "jquery.json-viewer.js")))
          (spit common-js (slurp (resource "common.js")))))
      (println "Done")))
  (shutdown-agents))