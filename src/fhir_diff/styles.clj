(ns fhir-diff.styles
  (:require [garden.core :refer [css]]))

(def style (atom {}))

(defn register-class [class-name-keyword style-obj]
  (swap! style assoc class-name-keyword style-obj))

(defn get-class [class-name-keyword]
  (str "." (name class-name-keyword)))

(defn get-styles []
  (css (for [[class styles-obj] @style]
         [(str "." (name class)) styles-obj])))