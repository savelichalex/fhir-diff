(ns fhir-diff.generate-page
  (:require [hiccup.page :refer [html5]]
            [clojure.string :refer [capitalize]]
            [fhir-diff.styles :as s]))

(defn create-breadcrumb [link]
  [:a.breadcrumb {:href (str link ".html")} (capitalize link)])

(s/register-class :logo {:margin-left "20px"})
(s/register-class :hidden {:display "none"})
(s/register-class :filter {:margin-bottom "0 !important"
                           :width "inherit !important"
                           :float "right !important"
                           :margin-right "1em !important"
                           :margin-top "1.2em !important"
                           :height "2em !important"})

(defmulti nav (fn [type & other] type))
(defmethod nav :default [type & other]
  [:nav
   (into [:div.nav-wrapper]
         (into [[:a.breadcrumb.logo {:href "index.html"} "FHIR diff"]]
               (map create-breadcrumb other)))])
(defmethod nav :with-filter [type & other]
  [:nav
   (into [:div.nav-wrapper]
         (into [[:a.breadcrumb.logo {:href "index.html"} "FHIR diff"] [:input.filter {:type "text" :placeholder "Filter"}]]
               (map create-breadcrumb other)))])

(defn css-link [href]
  [:link {:href href :rel "stylesheet" :type "text/css"}])
(defn js-link [href]
  [:script {:src href :type "text/javascript"}])
(defn css-raw [source]
  [:style {:type "text/css"} source])
(defn js-raw [source]
  [:script {:type "text/javascript"} source])

(def materialize-css-cdn "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css")
(def material-icons "http://fonts.googleapis.com/icon?family=Material+Icons")
(def materialize-js-cdn "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js")
(def jquery-js-cdn "https://code.jquery.com/jquery-2.2.4.min.js")
(def json-viewer-css "jquery.json-viewer.css")
(def json-viewer-js "jquery.json-viewer.js")

(def common-js-code "common.js")

(defn generate-head [& content]
  [:head content])
(defn generate-body [content]
  [:body content])

(defn generate-page [& content]
  (html5
    (generate-head
      (css-link materialize-css-cdn)
      (css-link material-icons)
      (css-link json-viewer-css)
      (css-raw (s/get-styles)))
    (generate-body
      (concat content
            [(js-link jquery-js-cdn)
             (js-link materialize-js-cdn)
             (js-link json-viewer-js)
             (js-link common-js-code)]))))

(defn gen-collapsibe-row [gen-row-header gen-row-body entry]
  [:li
   [:div.collapsible-header.contain-json-view
    (gen-row-header entry)]
   [:div.collapsible-body
    (gen-row-body entry)]])

(defn gen-collapsible [entries gen-row-header gen-row-body]
  (into [:ul.collapsible {:data-collapsible "accordion"}]
        (->> entries
             (map (partial gen-collapsibe-row gen-row-header gen-row-body)))))