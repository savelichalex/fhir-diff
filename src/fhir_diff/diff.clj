(ns fhir-diff.diff
  (:require [clojure.set :as set]
            [clojure.data :as data]))

(defn entry-vector-to-map [by-key entry-vector]
  (into {}
        (for
          [entry entry-vector]
          [(get-in entry by-key) entry])))
(def entry-vector-to-map-by-id (partial entry-vector-to-map ["resource" "id"]))

(defn entry-vector-to-map-with-vectors [key entry-vector]
  (->> (for [entry entry-vector] [(get-in entry key) entry])
       (reduce (fn [acc [key entry]]
                 (if (contains? acc key)
                   (update acc key conj entry)
                   (assoc acc key [entry]))) {})))
(def entry-vector-to-map-by-resource-type (partial entry-vector-to-map-with-vectors ["resource" "resourceType"]))

(defn entry-not-equal? [new-entry old-entry]
  (and
    (not (nil? new-entry))
    (not (nil? old-entry))
    (not= new-entry old-entry)))

(defn diff-entry [new-entry old-entry]
  (let [[from-new from-old] (data/diff new-entry old-entry)]
    {:difference-in-new from-new
     :difference-in-old from-old
     ;:original-old old-entry
     :original-new new-entry
     }))

(defn shallow-diff-by-ids [new-entry old-entry]
  (let [new-entry-map (entry-vector-to-map-by-id new-entry)
        old-entry-map (entry-vector-to-map-by-id old-entry)
        new-ids (into #{} (keys new-entry-map))
        old-ids (into #{} (keys old-entry-map))
        created-ids (set/difference new-ids old-ids)
        deleted-ids (set/difference old-ids new-ids)
        maybe-updated-ids (set/intersection old-ids new-ids)]
    {:created (->> created-ids
                   (map #(get new-entry-map %)))
     :deleted (->> deleted-ids
                   (map #(get old-entry-map %)))
     :updated (->> maybe-updated-ids
                   (reduce #(let [from-new-entry (get new-entry-map %2)
                                  from-old-entry (get old-entry-map %2)]
                             (if (entry-not-equal? from-new-entry from-old-entry)
                               (conj %1 (diff-entry from-new-entry from-old-entry))
                               %1)) []))
     }))

(defn get-resources-diff [new-resources old-resources]
  (let [new-resources-map (entry-vector-to-map-by-resource-type (flatten new-resources))
        old-resources-map (entry-vector-to-map-by-resource-type (flatten old-resources))
        new-resources-types (into #{} (keys new-resources-map))
        old-resources-types (into #{} (keys old-resources-map))
        created-resources-types (set/difference new-resources-types old-resources-types)
        deleted-resources-types (set/difference old-resources-types new-resources-types)
        same-resource-types (set/intersection new-resources-types old-resources-types)]
    {:created (into {}
                    (for
                      [type created-resources-types]
                      [type (get new-resources-map type)]))
     :deleted (into {}
                    (for
                      [type deleted-resources-types]
                      [type (get old-resources-map type)]))
     :updated (into {}
                    (for
                      [type same-resource-types]
                      [type (shallow-diff-by-ids (get new-resources-map type) (get old-resources-map type))]))}))

(def CONCEPTMAPS_FILE_NAME "conceptmaps")
(def V2-TABLES_FILE_NAME "v2-tables")
(def V3-CODESYSTEMS_FILE_NAME "v3-codesystems")
(def VALUESETS_FILE_NAME "valuesets")

(defn get-fhir-diff [get-entry-from-new get-entry-from-old]
  (let [new-conceptmaps (future (get-entry-from-new CONCEPTMAPS_FILE_NAME))
        old-conceptmaps (future (get-entry-from-old CONCEPTMAPS_FILE_NAME))
        new-v2-tables (future (get-entry-from-new V2-TABLES_FILE_NAME))
        old-v2-tables (future (get-entry-from-old V2-TABLES_FILE_NAME))
        new-v3-codesystems (future (get-entry-from-new V3-CODESYSTEMS_FILE_NAME))
        old-v3-codesystems (future (get-entry-from-old V3-CODESYSTEMS_FILE_NAME))
        new-valuesets (future (get-entry-from-new VALUESETS_FILE_NAME))
        old-valuesets (future (get-entry-from-old VALUESETS_FILE_NAME))
        conceptmaps-diff (future (shallow-diff-by-ids @new-conceptmaps @old-conceptmaps))
        v2-tables-diff (future (shallow-diff-by-ids @new-v2-tables @old-v2-tables))
        v3-codesystems-diff (future (shallow-diff-by-ids @new-v3-codesystems @old-v3-codesystems))
        valuesets-diff (future (shallow-diff-by-ids @new-valuesets @old-valuesets))
        resources-diff (future (get-resources-diff
                                 [@new-conceptmaps @new-v2-tables @new-v3-codesystems @new-valuesets]
                                 [@old-conceptmaps @old-v2-tables @old-v3-codesystems @old-valuesets]))
        ]
    {:conceptmaps @conceptmaps-diff
     :v2-tables @v2-tables-diff
     :v3-codesystems @v3-codesystems-diff
     :valuesets @valuesets-diff
     :resources @resources-diff
     }))