(ns fhir-diff.regular-page
  (:require [fhir-diff.generate-page :as gp]
            [clj-json.core :as json]
            [fhir-diff.styles :as s]))

(s/register-class :json-view {:margin-left "5%"
                              :margin-right "5%"})

(defn gen-created [diff]
  (gp/gen-collapsible
    (:created diff)
    #(identity [:span (get-in % ["resource" "id"])])
    #(identity [:pre.json-view {:data-json (json/generate-string %)}])))

(defn gen-deleted [diff]
  (gp/gen-collapsible
    (:deleted diff)
    #(identity [:span (get-in % ["resource" "id"])])
    #(identity [:pre.json-view {:data-json (json/generate-string %)}])))

(s/register-class :updated-inner-header {:padding "0"
                                         :margin-left "5%"
                                         :margin-right "5%"
                                         :padding-bottom "0.2em"
                                         :border-bottom "1px solid rgba(0,0,0,.17)"
                                         :font-size "1.5em"
                                         :font-weight "bold"
                                         :color "rgba(0,0,0,.3)"})

(defn gen-updated [diff]
  (gp/gen-collapsible
    (:updated diff)
    #(identity [:span (get-in % [:original-new "resource" "id"])])
    #(identity
      [:div
       [:h5.updated-inner-header "In new"]
       [:pre.json-view {:data-json (json/generate-string (:difference-in-new %))}]
       [:h5.updated-inner-header "In old"]
       [:pre.json-view {:data-json (json/generate-string (:difference-in-old %))}]])))

(s/register-class :status-header {:font-size "2.5em"
                                  :padding-left "5%"
                                  :margin-bottom 0
                                  :margin-top 0
                                  :font-weight "bold"
                                  :color "white"
                                  :text-shadow "1px 1px 5px rgba(0,0,0,.7)"})
(s/register-class :first-header {:margin-top "0.5em"})

(defn gen-regular-diff-markup [diff]
  [:div
   [:p.status-header.first-header "Created"]
   [:div.list-wrapper
    (gen-created diff)]
   [:p.status-header "Deleted"]
   [:div.list-wrapper
    (gen-deleted diff)]
   [:p.status-header "Updated"]
   [:div.list-wrapper
    (gen-updated diff)]])

(defn generate-regular-page [diff-type diff]
  (gp/generate-page
    (gp/nav :with-filter diff-type)
    (gen-regular-diff-markup diff)))