(ns fhir-diff.main-page
  (:require [fhir-diff.generate-page :as gp]
            [fhir-diff.styles :as s]))

(s/register-class :label-wrapper {:float "right"})
(s/register-class :label {:border-radius "3px"
                          :display "inline-block"
                          :color "white"
                          :padding-left "10px"
                          :padding-right "10px"
                          :margin-left "10px"})
(s/register-class :list-wrapper {:padding-left "5%"
                                 :padding-right "5%"})

(defn statistics-row [{:keys [title route created-count deleted-count updated-count ]}]
  [:a.collection-item {:href (str route ".html")}
   [:span title]
   [:div.label-wrapper
    [:span.label.green (str "Created " created-count)]
    [:span.label.red (str "Deleted " deleted-count)]
    [:span.label.blue (str "Updated " updated-count)]]])

(defn statistics [stats]
  [:div.list-wrapper
   (into
     [:div.collection]
     (->> stats
          (map statistics-row)))])

(defn calculate-stats-from-diff [diff]
  (into []
        (for [[key val] diff]
          (-> {:title (name key)
               :route (name key)}
              (merge (if (not= key :resources)
                       {:created-count (count (:created val))
                        :deleted-count (count (:deleted val))
                        :updated-count (count (:updated val))
                        }
                       {:created-count (count (keys (:created val)))
                        :deleted-count (count (keys (:deleted val)))
                        :updated-count (count (keys (:updated val)))}))))))

(defn generate-main-page [diff]
  (gp/generate-page
    (gp/nav :default)
    (statistics (calculate-stats-from-diff diff))))